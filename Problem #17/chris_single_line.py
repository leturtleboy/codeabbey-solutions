from functools import reduce

print([input(), reduce(lambda x,y: (int(x) + int(y)) * 113 % 10000007, [int(x) for x in ("0 " + input()).split(' ')])][1])
